<?php

namespace App;


use Illuminate\Database\Eloquent\Model;

class Empleado extends Model
{
    protected $fillable = ['codigo','nombre','apellido','puesto_id', 'numero_fijo','numero_celular',
    'direccion','dui','correo_electronico','fecha_de_nacimiento'];
    
    public function puesto()
    {
        return $this->belongsTo(Puesto::class);
    }



}
