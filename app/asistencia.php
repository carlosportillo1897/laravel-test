<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class asistencia extends Model
{
    protected $fillable = ['codigo','fecha_laboral','hora_laboral'];
 
    public function puesto()
    {
        return $this->belongsTo(Empleado::class);
    }
}
