<?php

namespace App\Http\Controllers;

use App\Empleado;
use App\Http\Requests\CreateEmpleadoRequest;
use App\Http\Requests\EditEmpleadoRequest;
use App\Puesto;
use Illuminate\Http\Request;

class EmpleadoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $empleados = Empleado::join("puestos","puestos.id","=","empleados.puesto_id" )
            ->select("empleados.id","empleados.codigo","empleados.nombre","empleados.apellido","puestos.name as puesto")
            ->get();
        

        return view('empleados.index',['empleados'=>$empleados]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $puestos= \App\Puesto::all();

        return view ('empleados.create', ['puestos'=>$puestos]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateEmpleadoRequest $request)
    {
        /*Empleado::create([
            'nombre'=>$request->nombre,
            'apellido'=>$request->apellido,
            'puesto_id'=>$request->puesto_id,
            'numero_fijo'=>$request->numero_fijo,
            'numero_celular'=>$request->numero_celular,
            'direccion'=>$request->direccion,
            'dui'=>$request->dui,
            'correo_electronico'=>$request->correo_electronico,
            'fecha_de_nacimiento'=>$request->fecha_de_nacimiento,
        ]);*/
        Empleado::create($request->validated());
   
        return redirect()->route('empleados.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Empleado  $empleado
     * @return \Illuminate\Http\Response
     */
    public function show(Empleado $empleado)
    {
        $puestos= \App\Puesto::all();
        return view('empleados.show',['puestos'=>$puestos], ['empleado'=>$empleado]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Empleado  $empleado
     * @return \Illuminate\Http\Response
     */
    public function edit(Empleado $empleado)
    {
        $puestos= \App\Puesto::all();
        return view ('empleados.edit', ['puestos'=>$puestos], ['empleado'=>$empleado]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Empleado  $empleado
     * @return \Illuminate\Http\Response
     */
    public function update(EditEmpleadoRequest $request, Empleado $empleado)
    {
       /* $request->validate([
            'nombre'=>'required',
            'apellido'=>'required',
            'puesto_id'=>'required',
            'numero_fijo'=>'required',
            'numero_celular'=>'required',
            'direccion'=>'required',
            'dui'=>'required',
            'correo_electronico'=>'required',
            'fecha_de_nacimiento'=>'required',
        ]);*/

        $empleado->update($request->validated());
  
        return redirect()->route('empleados.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Empleado  $empleado
     * @return \Illuminate\Http\Response
     */
    public function destroy(Empleado $empleado)
    {
        $empleado->delete();
  
        return redirect()->route('empleados.index')->with('eliminar','okey');
    }
}
