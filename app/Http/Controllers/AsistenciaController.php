<?php

namespace App\Http\Controllers;

use App\asistencia;
use App\Empleado;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Symfony\Component\Console\Input\Input;

class AsistenciaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    } 

    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view ('asistencias.create');

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

         $codigo = $request->get('codigo');
         $empleado = Empleado::where('codigo','=',"$codigo")->count();

         if (empty($empleado)){
            return 'Codigo no encontrado';
         }

         else{
            $date = Carbon::now();
            $fecha_actual =$date->toDateString();
            $hora_actual =$date->toTimeString();
   
            asistencia::create([
               'codigo'=>$request->codigo,
               'fecha_laboral'=>$fecha_actual,
               'hora_laboral'=>$hora_actual,
           ]);
           
           return back()->with(['mensaje' => 'Asistencia con exito']);
         }
         

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
