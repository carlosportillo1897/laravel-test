<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Routing\Route;
use Illuminate\Validation\Rule;

class EditEmpleadoRequest extends FormRequest
{
    
    protected $route;
    public function __construct(Route $route){
        $this->route=$route;
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'codigo'=>[ 'required',Rule::unique('empleados')->ignore($this->route->parameter('empleado'))],
            'nombre'=>'required|max:25|min:1',
            'apellido'=>'required|max:25|min:1',
            'puesto_id'=>'required',
            'numero_fijo'=>'required',
            'numero_celular'=>'required',
            'direccion'=>'required|max:50|min:5',
            'dui'=>[ 'required', 'max:10',Rule::unique('empleados')->ignore($this->route->parameter('empleado'))],
            'correo_electronico'=>['required','email',Rule::unique('empleados')->ignore($this->route->parameter('empleado'))],
            'fecha_de_nacimiento'=>'required',
        ];
    }

    public function messages()
 
    {
        return [
            'nombre.required'=>'Debe ingresar el nombre del Empleado',
            'nombre.max'=>'El campo nombre solo acepta 25 caracteres como maximo',
            'nombre.min'=>'El campo nombre debe tener como minimo 1 caracteres',
            'apellido.required'=>'Debe ingresar el apellido del Empleado',
            'apellido.max'=>'El campo apellido solo acepta 25 caracteres como maximo',
            'apellido.min'=>'El campo apellido debe tener como minimo 1 caracteres',
            'puesto_id.required'=>'Debe seleccionar el puesto del Empleado',
            'numero_fijo.required'=>'Debe ingresar el numero fijo',
            'numero_celular.required'=>'Debe ingresar el numero de celular',
            'direccion.required'=>'Debe ingresar la direccion',
            'direccion.max'=>'El campo direccion solo acepta 50 caracteres como maximo',
            'direccion.min'=>'El campo direccion debe tener como minimo 10 caracteres',
            'dui.required'=>'Debe ingresar el DUI',
            'correo_electronico.required'=>'Debe ingresar el correo electronico',
            'correo_electronico.email'=>'El formato es incorrecto como puede tomar ejemplo@email.com',
            'fecha_de_nacimiento.required'=>'Debe ingresar la fecha de Nacimiento',
            'dui.unique'=>'El DUI ingresado esta registrado intente con otro',
            'correo_electronico.unique'=>'El correo ingresado esta registrado intente con otro'
        ];
    }
}
