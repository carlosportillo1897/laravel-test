@extends('layouts.app')
@section('title', 'Empleados')
@section('content')
<div class="grid-container">

    <div class="grid-titulo-inicial">
        <h3>Listado de Empleados</h3>
    </div>

    <div class="grid-buttom-index">
        <a class="btn btn-primary botton-general" href="{{ route('empleados.create') }}"> Agregar un Nuevo Empleado</a>
        <a  type="submit"  class="btn btn-primary botton-general" href="{{ route('asistencias.create') }}">Asistencia</a>
    </div>

    <div class="grid-table">  
            @if ($errors->any())
                <div class="alert alert-danger">
                    <strong>¡Alerta!</strong> Se tienen un problema con los siguientes campos:
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif

            @section('CSS')
            <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.2/css/bootstrap.css">
            <link rel="stylesheet" href="https://cdn.datatables.net/1.12.0/css/dataTables.bootstrap4.min.css">
            @endsection

            <table id="empleado" class="table table-striped table-bordered" style="width:100%">
                    <thead>
                        <tr>
                            <th>Codigo</th>
                            <th>Nombre</th>
                            <th>Apellido</th>
                            <th>Puesto</th>
                            <th>Acciones</th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach ($empleados as $empleado)
                        <tr>
                            <td>{{$empleado->codigo}}</td>
                            <td>{{$empleado->nombre}}</td>
                            <td>{{$empleado->apellido}}</td>
                            <td>{{$empleado->puesto}}</td>
                            <td>
                                <div class="row">
                                    <a  type="submit"  class="btn btn-sm btn-primary botton-general" href="{{ route('empleados.edit',$empleado->id) }}">Editar</a>
                                    <a  type="submit"  class="btn btn-sm btn-secondary botton-general"  href="{{ route('empleados.show',$empleado->id) }}">Mostrar</a>
                                    <form action="{{route('empleados.destroy', $empleado)}}" id="test"  method="POST" >
                                        @method('DELETE')
                                        @csrf
                                        <button type="submit" class="btn btn-sm btn-danger botton-general" onclick="return confirm('¿Quiere eliminar este empleado?')">Eliminar</button>
                                    </form>
                                </div>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>

            @section('js')

                <script src="https://code.jquery.com/jquery-3.5.1.js"></script>
                <script src="https://cdn.datatables.net/1.12.0/js/jquery.dataTables.min.js"></script>
                <script src="https://cdn.datatables.net/1.12.0/js/dataTables.bootstrap4.min.js"></script>    

                <script>
                    $(document).ready(function () {
                    $('#empleado').DataTable();
                    });
                </script>
            @endsection            
    </div>  
</div>
@endsection