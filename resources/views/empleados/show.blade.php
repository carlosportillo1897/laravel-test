@extends('layouts.app')
@section('title', 'Datos del Empleado')
@section('content')
<div class="grid-container">

    <div class="grid-item">

        <h2>Datos del empleado</h2>
        
        <form action="{{route('empleados.show', $empleado->id)}}" method="POST">
            @csrf
            @method('PUT') 

            <a class="btn btn-success" href="{{route('empleados.index')}}">Regresar</a>
            
            <label class="form-label">Codigo</label>
            <label>{{$empleado->codigo}}</label>

            <label class="form-label">Nombre</label>
            <label>{{$empleado->nombre}}</label>

            <label class="form-label">Apellido</label>
            <label>{{$empleado->apellido}}</label>

                

            <div class="form-group">
                <label for="exampleFormControlInput1" class="form-label">Puesto de trabajo</label>
                    <select disabled class="form-control selectorWapis" id="puesto" name="puesto_id">
                        <option value="{{$empleado->puesto_id}}" selected disabled hidden>Seleccione el puesto *</option>
                        @foreach ($puestos as $puesto)
                            @if ($empleado->puesto_id==$puesto->id)                                     
                                <option  value="{{$puesto->id}}" selected>{{ $puesto->name }}</option>
                            @else
                                <option  value="{{$puesto->id}}">{{ $puesto->name }}</option>
                            @endif                                
                        @endforeach
                    </select>
            </div>
            <div class="form-group">
                <label class="form-label">Telefono</label>
                <input disabled type="Number" class="form-control"name="numero_fijo" id="numero_fijo" placeholder="22222222" 
                value="{{$empleado->numero_fijo}}">
            </div>
            <div class="form-group">
                <label class="form-label">Telefono Celular</label>
                <input  disabled type="Number" class="form-control" name="numero_celular" id="numero_celular" placeholder="77777777" 
                value="{{$empleado->numero_celular}}">
            </div>
            <div class="form-group">
                <label class="form-label">Direccion</label>
                <input disabled type="text" class="form-control" name="direccion" id="direccion" placeholder="Calle San Antonio Colonia San Jacinto" 
                value="{{$empleado->direccion}}">
            </div>
            <div class="form-group">
                <label class="form-label">DUI</label>
                <input disabled type="text" class="form-control" maxlength="10" name="dui" id="dui" placeholder="012345678-9" 
                value="{{$empleado->dui}}">
            </div>
            <div class="form-group">
                <label class="form-label">Email address</label>
                <input disabled type="email" class="form-control" name="correo_electronico" id="correo_electronico" placeholder="ejemplo@example.com" 
                value="{{$empleado->correo_electronico}}">
            </div>
            <div class="form-group">
                <label class="form-label">Fecha de Nacimiento</label>
                <input disabled type="Date" class="form-control" name="fecha_de_nacimiento" id="fecha_de_nacimiento" placeholder="DD/MM/AAAA" 
                value="{{$empleado->fecha_de_nacimiento}}">
            </div>         
              
        </form>
    </div>
</div> 

@endsection
