@extends('layouts.app')
@section('title', 'Crear Nuevo Empleados')
@section('content')
<div class="grid-container">
    @if ($errors->any())
        <div class="alert alert-danger">
            <strong>¡Alerta!</strong> Se tienen un problema con los siguientes campos:
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    <div class="grid-item">
        <form action="{{ route('empleados.store') }}" method="POST">
            @csrf
                <h2>Agregar un nuevo empleado</h2>
            <div class="form-group">
                <label class="form-label">Codigo</label>
                <input type="text" class="form-control" name="codigo" id="codigo" placeholder="Ingrese su codigo" value="{{old('codigo')}}">
            </div>
            <div class="form-group">
                <label class="form-label">Nombre</label>
                <input type="text" class="form-control" name="nombre" id="nombre" placeholder="Ingrese su nombre" value="{{old('nombre')}}">
            </div>
            <div class="form-group">
                <label class="form-label">Apellido</label>
                <input type="text" class="form-control" name="apellido" id="apellido" placeholder="Ingrese su Apellido" value="{{old('apellido')}}">
            </div>
        
            <div class="form-group">
                <label class="form-label">Puesto de trabajo</label>
                    <select class="form-control selectorWapis" id="puesto" name="puesto_id">
                        <option value="" selected disabled hidden>Seleccione el puesto *</option>
                        @foreach ($puestos as $puesto)
                            @if (old('puesto')==$puesto->id)                                     
                                <option  value="{{$puesto->id}}" selected>{{ $puesto->name }}</option>
                            @else
                                <option  value="{{$puesto->id}}">{{ $puesto->name }}</option>
                            @endif                                
                        @endforeach
                    </select>
            </div>
            <div class="form-group">
                <label class="form-label">Telefono</label>
                <input type="Number" class="form-control"name="numero_fijo" id="numero_fijo" placeholder="22222222" value="{{old('numero_fijo')}}">
            </div>
            <div class="form-group">
                <label class="form-label">Telefono Celular</label>
                <input type="Number" class="form-control" name="numero_celular" id="numero_celular" placeholder="77777777" value="{{old('numero_celular')}}">
            </div>
            <div class="form-group">
                <label class="form-label">Direccion</label>
                <input type="text" class="form-control" name="direccion" id="direccion" placeholder="Calle San Antonio Colonia San Jacinto" value="{{old('direccion')}}">
            </div>
            <div class="form-group">
                <label class="form-label">DUI</label>
                <input type="text" class="form-control" maxlength="10" name="dui" id="dui" placeholder="012345678-9" value="{{old('dui')}}">
            </div>
            <div class="form-group">
                <label class="form-label">Email address</label>
                <input type="email" class="form-control" name="correo_electronico" id="correo_electronico" placeholder="ejemplo@example.com" value="{{old('correo_electronico')}}">
            </div>
            <div class="form-group">
                <label class="form-label">Fecha de Nacimiento</label>
                <input type="Date" class="form-control" name="fecha_de_nacimiento" id="fecha_de_nacimiento" placeholder="DD/MM/AAAA" value="{{old('fecha_de_nacimiento')}}">
            </div>
            <div class=" row grid-item buttom-center">
                <button type="submit" class="btn btn-primary botton-general">Guardar</button>          
                <a type="submit" class="btn btn-danger botton-general" href="{{route('empleados.index')}}">Cancelar</a>  
            </div>
        </form>
    </div>

</div>

            

           

@endsection
