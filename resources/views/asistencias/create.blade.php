@extends('layouts.app')
@section('title', 'Asistencia')
@section('content')
<div class="grid-container">

    <div class="grid-item">
        <h3>Asistencia</h3>
    </div>

    <div class="grid-item">
        @if ($errors->any())
            <div class="alert alert-danger">
                <strong>Whoops!</strong>Se tienen un problema con los siguientes campos:<br><br>
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        <form action="{{ route('asistencias.store') }}" method="POST">
                @csrf
            <div class="form-group">
                <label class="form-label">Codigo</label>
                <input type="text" class="form-control" name="codigo" id="codigo" placeholder="Ingrese su codigo">
            </div>
            <label for="">{{$mensaje ?? ''}}</label>

            <div class=" row grid-item buttom-center">
                <button type="submit" class="btn btn-primary botton-general">Guardar</button>          
                <a type="submit" class="btn btn-danger botton-general" href="{{route('empleados.index')}}">Cancelar</a>  
            </div> 
                
        </form>
        
        @section('scripts')
            <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
        
        @endsection
    </div>
</div>
@endsection