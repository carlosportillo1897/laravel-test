@extends('layouts.app')
@section('title', 'Asistencia')
@section('content')
<div class="content-header">
            <div class="pull-right">
                <h2>Asistencia</h2>
            </div>
    
  <div class="container-fluid">
        @if ($errors->any())
            <div class="alert alert-danger">
                <strong>Whoops!</strong>Se tienen un problema con los siguientes campos:<br><br>
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        @section('CSS')
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.2/css/bootstrap.css">
        <link rel="stylesheet" href="https://cdn.datatables.net/1.12.0/css/dataTables.bootstrap4.min.css">
        @endsection

       

        @section('js')
            <script src="https://code.jquery.com/jquery-3.5.1.js"></script>
            <script src="https://cdn.datatables.net/1.12.0/js/jquery.dataTables.min.js"></script>
            <script src="https://cdn.datatables.net/1.12.0/js/dataTables.bootstrap4.min.js"></script>

            <script>
                $(document).ready(function () {
                $('#empleado').DataTable();
                });
            </script>
        @endsection            
  </div>
</div>
@endsection