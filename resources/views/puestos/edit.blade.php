@extends('layouts.app')
@section('title', 'Editar Puesto')
@section('content')
<div class="grid-container">
    
    <div class="grid-item">
        <h3>Modificando un puesto</h3>
    </div>

    <div class="grid-item">
            @if ($errors->any())
                <div class="alert alert-danger">
                    <strong>¡Alerta!</strong> Se tienen un problema con los siguientes campos:
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
        <form action="{{route('puestos.update', $puesto->id)}}" method="POST">
            @csrf
            @method('PUT') 
                    <div class="form-group">
                        <label class="form-label">Nombre</label>
                        <input type="text" class="form-control" name="name" id="name" placeholder="Ingrese el nombre del puesto" 
                        value="{{$puesto->name}}">
                    </div>
            <div class=" row grid-item buttom-center">
                <button type="submit" class="btn btn-primary botton-general">Modificar</button>          
                <a class="btn btn-danger botton-general" href="{{route('puestos.index')}}">Cancelar</a>      
            </div>          
        </form>
    </div>
</div>
@endsection
