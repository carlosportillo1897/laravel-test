@extends('layouts.app')
@section('title', 'Crear Nuevo Puesto')
@section('content')
<div class="content-header">
  <div class="container-fluid">
        <div class="container">
            @if ($errors->any())
                <div class="alert alert-danger">
                    <strong>¡Alerta!</strong> Se tienen un problema con los siguientes campos:
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif

            <form action="{{ route('puestos.store') }}" method="POST">
                @csrf
                <div class="pull-right">
                    <h2>Agregar un nuevo puesto</h2>
                </div>
                <div class="form-group">
                    <label class="form-label">Nombre</label>
                    <input type="text" class="form-control" name="name" id="name" placeholder="Ingrese su nombre del puesto" value="{{old('name')}}">
                </div>
            
                <button type="submit" class="btn btn-primary">Guardar</button>          
                <a type="submit" class="btn btn-danger" href="{{route('puestos.index')}}">Cancelar</a>  
            </form>
        </div>
  </div>
</div>
@endsection