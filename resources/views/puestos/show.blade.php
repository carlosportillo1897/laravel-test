@extends('layouts.app')
@section('title', 'Datos del Puesto')
@section('content')
<div class="content-header">
  <div class="container-fluid">
    <div class="container">
            <div class="pull-right">
                <h2>Datos del puesto</h2>
            </div>
            <form action="{{route('puestos.show', $puesto->id)}}" method="POST">
                @csrf
                @method('PUT') 
            
                <div class="form-group">
                    <label class="form-label">Nombre</label>
                    <input disabled type="text" class="form-control" name="name" id="name" placeholder="Ingrese su nombre" 
                    value="{{$puesto->name}}">
                </div>        
                <a class="btn btn-success" href="{{route('puestos.index')}}">Regresar</a>  
            </form>
    </div> 
  </div>
</div>
@endsection
